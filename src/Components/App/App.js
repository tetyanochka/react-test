import React, { Component } from 'react';
import './App.css';

import Exercises from './../Exercises/Exercises';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Exercises />
      </div>
    );
  }
}

export default App;
