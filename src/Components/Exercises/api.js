import axios from 'axios';

let api = {
  getExercises: function getExercises(limit, offset) {
    const URL =  "http://dev.videotherapy.co/vt/api/dispatcher.php";

    return axios.post(URL, {
      "api": "exercises/get-exercises",
      "clientKey": "8e28b8db-6395-4417-9df9-10dd0efb5ef9",
      "paging": {"limit": limit, "offset": offset}
    }).then(response => {
      return response.data
    });
  }
};

export default api;
