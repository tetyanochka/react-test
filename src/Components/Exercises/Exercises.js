import React, { Component } from 'react';

import FilteredList from '../FilteredList/FilteredList';
import api from './api';

class Exercises extends Component {

  constructor(props) {
    const defaults = {filterField: "BodyZoneJson", filterFieldTitle: "Body Zone", filterValue: 1};
    super(props);

    this.props = props;
    this.isLoading = true;
    this.filterField = this.props.filterField || defaults.filterField;
    this.filterFieldTitle = this.props.filterFieldTitle || defaults.filterFieldTitle;

    this.state = {
      items: [],
      filterValue: this.props.defaultFilterValue || defaults.filterValue
    };
  }

  componentDidMount() {
    const limit = 12;
    let offset = 0;

    api.getExercises(limit, offset).then((res) => {
      const total = res.totalRows;
      let exercises = res.data;

      const onExersisesCollected = ()=> {
        this.isLoading = false;
        this.setState({items: exercises});
      };

      if (total > exercises.length) {
        let promises = [];
        while ((offset + limit) < total) {
          offset += limit;
          promises.push(api.getExercises(limit, offset));
        }

        Promise.all(promises).then((resp) => {
          resp.forEach((item)=> {
            exercises = exercises.concat(item.data)
          });
          onExersisesCollected();
        })

      } else {
        onExersisesCollected();
      }
    });

  }


  render() {
    return (
      <div>
        <h1>Exercises</h1>
        <FilteredList isLoading={this.isLoading}
                      filterField={this.filterField} filterFieldTitle={this.filterFieldTitle}
                      filterValue={this.state.filterValue}
                      items={this.state.items}
        />
      </div>
    );
  }
}

export default Exercises;
