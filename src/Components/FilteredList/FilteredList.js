import React, { Component } from 'react';

class Item extends Component {
  render() {
    return <li>{this.props.label}</li>
  }
}

class ItemsList extends Component {
  render() {
    const list = this.props.items;
    const total = list.length;

    return (
      <div>
        <strong>Found {total}:</strong>
        <ol>
          {list.map((item) => <Item key={item.id} label={item.label} />)}
        </ol>
      </div>
    )
  }
}

class FilterBar extends Component {
  constructor(props) {
    super(props);
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
  }

  handleFilterTextChange(e) {
    this.props.onFilterTextChange(e.target.value);
  }

  render() {
    const title = this.props.title;
    return (
      <form>
        <label>Filter By {title}</label>
        <input type="number" placeholder="Type value..."
               value={this.props.filterText} onChange={this.handleFilterTextChange}
        />
      </form>
    );
  }
}

class FilteredList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: this.props.filterValue
    };

    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
  }

  handleFilterTextChange(filterText) {
    this.setState({
      filterText: filterText ? parseFloat(filterText) : filterText
    });
  }

  render() {
    const items = !this.state.filterText ? this.props.items : this.props.items.filter((elem)=> {
      let item = elem[this.props.filterField];
      return item && (item.indexOf(this.state.filterText) !== -1)
    });

    if (this.props.isLoading) {
      return <div>Loading...</div>
    }

    return (
      <div>
        <FilterBar filterField={this.props.filterField} filterText={this.state.filterText}
                   title={this.props.filterFieldTitle}
                   onFilterTextChange={this.handleFilterTextChange}
        />

        <ItemsList items={items} />

      </div>
    );
  }
}


export default FilteredList;
